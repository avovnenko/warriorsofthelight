# Warriors of the Light

Last stand strategy game. 

The Dark comes! This world has no sun and the only light sources are the Trees. 

Goblins and other evil creatures destroed most of them.

You should protected your Great Tree and save the Light!


![Game screen](/screens/gameplay.png)

Used assets:

* Standart terrain asset
* Wispy Skybox (https://www.assetstore.unity3d.com/en/#!/content/21737)
* Fire and Spell Effects (https://www.assetstore.unity3d.com/en/#!/content/36825)
* Icons (http://www.freepik.com)
* Character models (mixamo.com)