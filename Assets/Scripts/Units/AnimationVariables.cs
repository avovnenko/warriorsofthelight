﻿namespace Assets.Scripts.Units
{
    public class AnimationVariables
    {
        public const string Speed = "speed";
        public const string Attack = "attack";
        public const string Cast = "cast";
    }
}